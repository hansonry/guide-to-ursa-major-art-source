# Guide to Ursa Major Art Source

This is for the art asset source for the https://gitlab.com/hansonry/guide-to-ursa-major repository.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />All non refrence work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## Usage

Commit all art source files to this repository instead of the guide-to-ursa-major repository. 
Any assets in their final form may be committed to the master repository for integration.

Types of "Art":
* Images
* 3D Models
* Sounds
* Refrences

## Rational

Art source files can be pretty big and change a lot. Instead of creating massive dowloads of 
the source code, instead this repository can be used freely without consern. 
